/*
* Description:
* Author: Hesam Sehat, sehathesam@gmail.com
* Creation Data: 2019-06-24
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FComponents.Player;
using UnityEngine;
using Revy.Framework;
using TMPro;
using UnityEngine.UI;

public class InGameUIController : FComponent, IInitializable, IInjectable, ITick
{
    #region Fields

    //------------Revy field-----------------
#pragma warning disable 649
    [CInject] private IPoolingSystem _poolingSystem;
    [CInject] private IEventSystem _eventSystem;
    [CInject] private IStateSystem _stateSystem;
    [CInject] private IZombieXResourceGameSystem _zombieXResourceGameSystem;
#pragma warning restore 649

    //-------------Inspector field------------
    [SerializeField] private Canvas canvas;
    [SerializeField] private Image playerHealthBar;
    [SerializeField] private Transform pausePanel;
    [SerializeField] private TextMeshProUGUI fpsNumber;
    [SerializeField] private TextMeshProUGUI weaponAmmoTxt;
    [SerializeField] private Image weaponIcon;
    [SerializeField] private HealthBar healthBar;

    //-------------Private field--------------
    private bool _isBarrage;
    private Player _player;
    private PlayerProfile _playerProfile;
    private readonly Dictionary<IHealth, HealthBar> _zombieHealthBar = new Dictionary<IHealth, HealthBar>();
    private readonly Dictionary<IHealth, HealthBar> _interactiveObjectsHealthBar = new Dictionary<IHealth, HealthBar>();

    #endregion

    #region Properties

    public bool HasInitialized { get; set; }
    public bool IsActive { get; set; }
    

    #endregion

    #region Class Interface

    #endregion

    #region FComponent's Callbacks

    public void Initialize()
    {
    }

    public Task BeginPlay()
    {
        EventRegister();
        return Task.CompletedTask;
    }
	
	public void Tick()
    {
        if (_isBarrage && !_player.IsDead)
        {
            _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.Barrage);
            switch (_playerProfile.GetPlayerWeapons.currentWeaponDetails.weaponBasicType)
            {
                case EWeaponBasicType.MeleeWeapon:
                    break;
                case EWeaponBasicType.LightWeapon:
                    weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.lightWeapon.weaponClip.ToString();
                    break;
                case EWeaponBasicType.HeavyWeapon:
                    weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.heavyWeapon.weaponClip.ToString();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        foreach (var zHealthBar in _zombieHealthBar)
        {
            if (zHealthBar.Key.Health > 0)
            {
                var screenPoint = Camera.main.WorldToScreenPoint(zHealthBar.Key.HealthBarTransform.position);
                zHealthBar.Value.transform.position = screenPoint;
            }
        }   
        foreach (var healthBar in _interactiveObjectsHealthBar)
        {
            if (healthBar.Key.Health > 0)
            {
                var screenPoint = Camera.main.WorldToScreenPoint(healthBar.Key.HealthBarTransform.position);
                healthBar.Value.transform.position = screenPoint;
            }
        }

        TestFire();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        _eventSystem.RemoveAllListeners(this);
    }



    #endregion

    #region Helpers

    #region Event Registered

    private void EventRegister()
    {
        _eventSystem.ListenToEvent(ZombieXEvents.GamePlayEvents.PrepareToStartGame, OnPrepareToStartGame);
        _eventSystem.ListenToEvent<Player>(ZombieXEvents.PlayerEvents.PlayerCreated, OnPlayerCreated);
        _eventSystem.ListenToEvent<int>(ZombieXEvents.PlayerEvents.HitToPlayer, OnHitToPlayer);
        _eventSystem.ListenToEvent<EWeaponBasicType>(ZombieXEvents.WeaponEvents.WeaponChanged, OnWeaponChanged);
        _eventSystem.ListenToEvent<Zombie>(ZombieXEvents.ZombieEvents.ZombieInstantiated, OnZombieInstantiated);
        _eventSystem.ListenToEvent<Zombie>(ZombieXEvents.ZombieEvents.ZombieWereDamaged, OnZombieWereDamaged);
        _eventSystem.ListenToEvent<Zombie>(ZombieXEvents.ZombieEvents.ZombieDied, OnZombieDied);
        _eventSystem.ListenToEvent<InteractiveObject>(
            ZombieXEvents.InteractiveObjectEvents.InteractiveObjectInstantiated, OnInteractiveObjectInstantiated);
        _eventSystem.ListenToEvent<InteractiveObject>(
            ZombieXEvents.InteractiveObjectEvents.InteractiveObjectWereDamaged, OnInteractiveObjectWereDamaged);
        _eventSystem.ListenToEvent<InteractiveObject>(
            ZombieXEvents.InteractiveObjectEvents.InteractiveObjectIsDestroyed, OnInteractiveObjectIsDestroyed);
        _eventSystem.ListenToEvent(ZombieXEvents.CollectableObjectEvents.PlayerCollectHealthPack,OnPlayerCollectHealthPack);
        _eventSystem.ListenToEvent(ZombieXEvents.CollectableObjectEvents.PlayerCollectFullAmmo,OnPlayerCollectFullAmmo);
        _eventSystem.ListenToEvent(ZombieXEvents.CollectableObjectEvents.PlayerCollectHalAmmo,OnPlayerCollectHalAmmo);
        _eventSystem.ListenToEvent(ZombieXEvents.CollectableObjectEvents.PlayerCollectCoin,OnPlayerCollectCoin);
        _eventSystem.ListenToEvent(ZombieXEvents.CollectableObjectEvents.PlayerCollectGem,OnPlayerCollectGem);
        
    }

    #endregion

    #region Revy Event Handler

    [CExecutionOrder(0)]
    private Task OnPrepareToStartGame()
    {
        Init();
        return Task.CompletedTask;
    }

    private Task OnPlayerCreated(Player player)
    {
        _player = player;

        playerHealthBar.fillAmount = _player.Health / (float)_player.MaxHealth;
        return Task.CompletedTask;
    }

    private Task OnHitToPlayer(int damage)
    {
        playerHealthBar.fillAmount = _player.Health / (float)_player.MaxHealth;
        return Task.CompletedTask;
    }

    private async Task OnWeaponChanged(EWeaponBasicType weaponBasicType)
    {
        switch (weaponBasicType)
        {
            case EWeaponBasicType.MeleeWeapon:
                weaponIcon.sprite = _zombieXResourceGameSystem.WeaponIconSprites.Single(s =>
                    s.name == _playerProfile.GetPlayerWeapons.meleeWeapon.weaponDetails.name);
                weaponAmmoTxt.text = "----";
                break;
            case EWeaponBasicType.LightWeapon:
                weaponIcon.sprite = _zombieXResourceGameSystem.WeaponIconSprites.Single(s =>
                    s.name == _playerProfile.GetPlayerWeapons.lightWeapon.weaponDetails.name);
                weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.lightWeapon.weaponClip.ToString();
                break;
            case EWeaponBasicType.HeavyWeapon:
                weaponIcon.sprite = _zombieXResourceGameSystem.WeaponIconSprites.Single(s =>
                    s.name == _playerProfile.GetPlayerWeapons.heavyWeapon.weaponDetails.name);
                weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.heavyWeapon.weaponClip.ToString();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(weaponBasicType), weaponBasicType, null);
        }

        await Task.CompletedTask;
    }

    private Task OnZombieInstantiated(Zombie zombie)
    {
        if (!_zombieHealthBar.ContainsKey(zombie))
        {
            var healthBarPooled = _poolingSystem.GetItem(healthBar.GetComponent<PoolingObject>().PoolingName);
            healthBarPooled.transform.position = new Vector3(-1000, 0, 0);

            _zombieHealthBar.Add(zombie, healthBarPooled.GetComponent<HealthBar>());
        }
        else
        {
            _zombieHealthBar[zombie].transform.position = new Vector3(-10000, 0, 0);
        }

        return Task.CompletedTask;
    }

    private Task OnZombieWereDamaged(Zombie zombie)
    {
        _zombieHealthBar[zombie].SetHealthBar(zombie.Health, zombie.MaxHealth);
        return Task.CompletedTask;
    }

    private Task OnZombieDied(Zombie zombie)
    {
        _zombieHealthBar[zombie].Restart();
        return Task.CompletedTask;
    }


    private Task OnInteractiveObjectInstantiated(InteractiveObject interactiveObject)
    {
        if (!_interactiveObjectsHealthBar.ContainsKey(interactiveObject))
        {
            var healthBarPooled = _poolingSystem.GetItem(healthBar.GetComponent<PoolingObject>().PoolingName);
            healthBarPooled.transform.position = new Vector3(-1000, 0, 0);

            _interactiveObjectsHealthBar.Add(interactiveObject, healthBarPooled.GetComponent<HealthBar>());
        }
        else
        {
            _interactiveObjectsHealthBar[interactiveObject].transform.position = new Vector3(-10000, 0, 0);
        }

        return Task.CompletedTask;
    }

    private Task OnInteractiveObjectWereDamaged(InteractiveObject interactiveObject)
    {
        _interactiveObjectsHealthBar[interactiveObject]
            .SetHealthBar(interactiveObject.Health, interactiveObject.MaxHealth);
        return Task.CompletedTask;
    }

    private Task OnInteractiveObjectIsDestroyed(InteractiveObject interactiveObject)
    {
        _interactiveObjectsHealthBar[interactiveObject].Restart();
        return Task.CompletedTask;
    }

    private Task OnPlayerCollectHealthPack()
    {
        _player.SetFullHealth();
        playerHealthBar.fillAmount = _player.Health / (float)_player.MaxHealth;
        return Task.CompletedTask;
    }
    private Task OnPlayerCollectFullAmmo()
    {
        _playerProfile.SetFullWeaponClip();
        switch (_playerProfile.GetPlayerWeapons.currentWeaponDetails.weaponBasicType)
            {
                case EWeaponBasicType.MeleeWeapon:
                    break;
                case EWeaponBasicType.LightWeapon:
                    weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.lightWeapon.weaponClip.ToString();
                    break;
                case EWeaponBasicType.HeavyWeapon:
                    weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.heavyWeapon.weaponClip.ToString();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        return Task.CompletedTask;
    }
    private Task OnPlayerCollectHalAmmo()
    {
        _playerProfile.SetHalfWeaponClip();
        switch (_playerProfile.GetPlayerWeapons.currentWeaponDetails.weaponBasicType)
        {
            case EWeaponBasicType.MeleeWeapon:
                break;
            case EWeaponBasicType.LightWeapon:
                weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.lightWeapon.weaponClip.ToString();
                break;
            case EWeaponBasicType.HeavyWeapon:
                weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.heavyWeapon.weaponClip.ToString();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        return Task.CompletedTask;
    } 
    private Task OnPlayerCollectCoin()
    {
        return Task.CompletedTask;
    } 
    private Task OnPlayerCollectGem()
    {
        return Task.CompletedTask;
    }

    #endregion

    #region Private Function

    private  void Init()
    {
        _poolingSystem.SetItem("healthBar", healthBar.gameObject, 10, parent: canvas.transform, isExtendable: true);
        _playerProfile = _zombieXResourceGameSystem.PlayerProfile;
    }

    private async void FpsCalculate()
    {
        if (fpsNumber == null)
        {
            CLog.Warning("fpsNumber is null");
            return;
        }

        while (true)
        {
            fpsNumber.text = (1 / Time.smoothDeltaTime).ToString("0.0");
            await CoroutineToTaskAsync(new WaitForEndOfFrame());
        }
    }

    private void TestFire()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.HeavyWeaponBtnClicked);
        }
    }

    #endregion

    #region Public Function

    public void OnPointerDownShootBtn()
    {
        if (_playerProfile.GetPlayerWeapons.currentWeaponDetails.isAutomaticShoot)
            _isBarrage = true;
    }

    public void OnPointerUpShootBtn()
    {
        _isBarrage = false;
    }

    public void OnClickMeleeWeaponBtn()
    {
        if (!_player.IsDead)
            _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.MeleeWeaponBtnClicked);
    }

    public void OnClickLightWeaponBtn()
    {
        if (_player.IsDead) return;
        _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.LightWeaponBtnClicked);
        weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.lightWeapon.weaponClip.ToString();
    }

    public void OnClickHeavyWeaponBtn()
    {
        if (_player.IsDead) return;
        _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.HeavyWeaponBtnClicked);
        weaponAmmoTxt.text = _playerProfile.GetPlayerWeapons.heavyWeapon.weaponClip.ToString();
    }

    public void OnClickPauseBtn()
    {
        _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.PauseBtnClicked);
        pausePanel.gameObject.SetActive(true);
    }

    public void OnClickResumeBtn()
    {
        _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.ResumeBtnClicked);
        pausePanel.gameObject.SetActive(false);
    }

    public void OnClickRetryBtn()
    {
        _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.RestartBtnClicked);
        pausePanel.gameObject.SetActive(false);
    }

    public void OnClickQuitBtn()
    {
        _eventSystem.BroadcastEvent(ZombieXEvents.InGameUIEvents.BackToMainMenu);
        pausePanel.gameObject.SetActive(false);
    }

    #endregion

    #endregion
}